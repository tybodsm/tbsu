# README #

## tbsU ##

### STILL IN TESTING STAGES AS PIECES GET MOVED OVER

### Description ###

* This is an ever-growing collection of tools used by Tyler Bodine-Smith for data science Work.

* It is divided into 3 main modules:
  * sql
    * Tools for interacting with SQL databases (right now just postgres)
    * If you use a single postgres db often, this allows a quick and easy connection.
    * Ths may run on top of Ibis in the future
  
  * dfclean
    * Tools for cleaning and transforming datasets
    * Includes group_concord, a powerful tool for identifying distinct clusters given edges
      * e.g. if you have a m:m concordance, it will generate a unique id for all connected ids

  * alerts
    * Tools for setting up automatic alerts and other messages to be sent through slack.

## Set Up ##

### Prerequisite python packages: ###

 * numpy
 * pandas
 * scipy
 * sqlalchemy
 * tabulate

### Other prerequisites: ###
 * PSQL (and a postgres database) in order to use the sql module
   * If you require a postres password, you'll need to setup your .pgpass file

### Initial setup ###
 1. Download the repo
 2. Run `conda develop` on the folder, or add a .pth file to your site-packages folder with the repo location
 3. To use a default postgres engine, add the following to your `~/.bashrc` regarding your postgres db
 	```
 		export TBSU_DW_HOST=<insert postgres host location>
 		export TBSU_DW_NAME=<insert database name>
 		export TBSU_DW_USER=<insert username>
 	```
 4. Set up a slack webhook using `slack.com/apps/A0F7XDUAZ-incoming-webhooks` and store it in the alerts module using `store_channel()`

### Horizon Improvements ###
* Continue to move projects over into this repo

### Who do I talk to? ###

* Questions or comments can be directed to:
    * Tyler Bodine-Smith (tbs@tylerbs.com)